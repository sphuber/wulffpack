Icosahedral particle
====================

.. module:: wulffpack

.. index::
   single: Class reference; Icosahedron

.. autoclass:: Icosahedron
    :members:
    :undoc-members:
    :inherited-members:
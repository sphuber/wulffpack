Decahedral particle
===================

.. module:: wulffpack

.. index::
   single: Class reference; Decahedron

.. autoclass:: Decahedron
    :members:
    :undoc-members:
    :inherited-members:
